// soal 1 
console.log("SOAL 1");
const phi = 3.14;
function luaslingkaran(jari_jari) {
    return phi * jari_jari * jari_jari;
}
let luasLingkaran = luaslingkaran(5);
function kelilinglingkaran(jari_jari) {
    return phi * jari_jari * 2;
}
let kelilingLingkaran = luaslingkaran(5);
console.log("KELILING LINGKARAN :" + kelilingLingkaran);
console.log("lUAS LINGKARAN :" + luasLingkaran);
// soal 2 
console.log("SOAL 2");
function introduce(nama, umur, gender, pekerjaan) {
    return "Pak" + " " + nama + " " + "adalah seorang" + " " + pekerjaan + " " + gender + " " + "yang berusia" + " " + umur + " " + "tahun";
}
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis");
console.log(perkenalan);
// soal 3 
console.log("SOAL 3");
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
        }
    }
}

// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()
// soal 4 
console.log("SOAL 4 ");
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

const { name: phoneBrand, brand: phoneName, year, colors } = phone
const [colorBronze, colorWhite, colorBlack] = colors
// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze)
// soal 5 
console.log("SOAL 5")
let warna = ["biru", "merah", "kuning", "hijau"]

let dataBukuTambahan = {
    penulis: "john doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172,
    warnaSampul: ["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali
buku = {
    ...buku,
    ...dataBukuTambahan,

}
buku.warnaSampul = [...buku.warnaSampul, ...warna];
console.log(buku);
